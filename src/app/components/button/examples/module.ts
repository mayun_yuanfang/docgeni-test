import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TestButtonModule } from '@test/button';
import { BasicComponent } from './basic/basic.component';

const COMPONENTS = [BasicComponent];

@NgModule({
    declarations: COMPONENTS,
    imports: [CommonModule, TestButtonModule],
    exports: COMPONENTS,
    providers: []
})
export class AlibButtonExamplesModule {}
