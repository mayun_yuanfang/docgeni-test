import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

/**
 * General Button Component description.
 */
@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  public a = 1;

  /**
    * Button Type
    * @description Button Type for Description
    */
  @Input() thyType: 'primary' | 'info' | 'success' = 'primary';

  /**
   * Loading Event
   */
  @Output() thyLoadingEvent = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void { }

}
