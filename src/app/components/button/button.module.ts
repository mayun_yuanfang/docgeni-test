import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonComponent } from './button.component';
import { TestPipePipe } from './pipe/test-pipe.pipe';

@NgModule({
    declarations: [ButtonComponent, TestPipePipe],
    imports: [CommonModule],
    exports: [ButtonComponent]
})
export class ButtonModule { }
