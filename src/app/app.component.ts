import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 * General Button Component description.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-app';

  /**
     * Button Type
     * @description Button Type for Description
     */
  @Input() thyType?: 'primary' | 'info' | 'success' = 'primary';

  /**
  * Button Size
  * @deprecated
  * @default md
  */
  @Input() thySize?: 'lg' | 'md' | 'sm';


  /**
   * Loading Event
   */
  @Output() thyLoadingEvent = new EventEmitter<boolean>();
}
