/**
 * @type {import('@docgeni/core').DocgeniConfig}
 */
module.exports = {
    mode: 'full',
    title: 'my-app',
    description: '',
    docsDir: 'docs',
    navs: [
        null,
        {
          title: '组件',
          path: 'components',
          lib: 'alib',
      }
    ],
    libs: [
      {
        name: 'alib',
        rootDir: './',
        include: ['./src/app/components'],
        apiMode: 'automatic'
      }
    ],
};



